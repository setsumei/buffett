import argparse
from loguru import logger

def make_dataset(args):
    with open(args.data_path,"r") as f:
        datas = f.read().splitlines()
    gp_data = datas[0].strip().split()
    gp_id = gp_data[0].strip()
    gp_name = gp_data[1].strip()
    head = datas[1].strip().split()
    new_head = ",".join(head)  # CSV的题头
    new_data = "\n".join(datas[2:-1])
    csv = new_head + '\n' + new_data
    with open(f"datasets/{gp_id}_{gp_name}.csv","w",encoding="utf-8") as f:
        f.write(csv)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="处理数据集")
    parser.add_argument("-d","--data_path",type=str,help='日线文件名',required=True)
    args = parser.parse_args()
    make_dataset(args)

